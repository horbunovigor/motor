(function ($) {
  ("use strict");
  let layout = $(".layout"),
    header = $(".layout__header");
  preloader();
  function preloader() {
    layout.on("click", ".nav__link", function (event) {
      layout.removeClass("layout_ready-load");
      event.preventDefault();
      var linkLocation = this.href;
      setTimeout(function () {
        window.location = linkLocation;
      }, 500);
    });
    setTimeout(function () {
      layout.addClass("layout_ready-load");
    }, 0);
  }
  // Основной слайдер
  if ($(".layout__slider").length) {
    $(".layout__slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      fade: true,
      speed: 2000,
      autoplay: true,
      autoplaySpeed: 2000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 20" ><path d="M13.8423 17.5078C14.008 17.5076 14.1698 17.458 14.3072 17.3654C14.4446 17.2727 14.5513 17.1412 14.6135 16.9876C14.6758 16.8341 14.6909 16.6654 14.6569 16.5033C14.6228 16.3411 14.5412 16.1927 14.4225 16.0771L8.34502 9.99967L14.4225 3.9222C14.5025 3.84541 14.5663 3.75344 14.6103 3.65167C14.6543 3.54989 14.6775 3.44036 14.6787 3.3295C14.6798 3.21863 14.6588 3.10865 14.6169 3.006C14.575 2.90335 14.513 2.8101 14.4346 2.7317C14.3562 2.6533 14.263 2.59133 14.1603 2.54943C14.0577 2.50752 13.9477 2.48651 13.8368 2.48764C13.7259 2.48877 13.6164 2.51201 13.5146 2.556C13.4129 2.59998 13.3209 2.66384 13.2441 2.74382L6.57744 9.41048C6.42122 9.56677 6.33346 9.7787 6.33346 9.99967C6.33346 10.2207 6.42122 10.4326 6.57744 10.5889L13.2441 17.2555C13.3219 17.3355 13.4149 17.399 13.5176 17.4423C13.6203 17.4856 13.7307 17.5079 13.8423 17.5078Z"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 20" ><path d="M7.15775 2.49219C6.99204 2.49239 6.83015 2.54199 6.69277 2.63465C6.55539 2.72731 6.44875 2.85881 6.38647 3.01237C6.32419 3.16593 6.3091 3.33457 6.34313 3.49675C6.37715 3.65893 6.45876 3.80728 6.57751 3.92285L12.655 10.0003L6.57751 16.0778C6.49753 16.1546 6.43368 16.2466 6.38969 16.3483C6.3457 16.4501 6.32246 16.5596 6.32133 16.6705C6.3202 16.7814 6.34121 16.8914 6.38312 16.994C6.42502 17.0966 6.48699 17.1899 6.56539 17.2683C6.64379 17.3467 6.73704 17.4087 6.83969 17.4506C6.94234 17.4925 7.05232 17.5135 7.16319 17.5124C7.27406 17.5112 7.38358 17.488 7.48536 17.444C7.58713 17.4 7.67911 17.3362 7.75589 17.2562L14.4226 10.5895C14.5788 10.4332 14.6665 10.2213 14.6665 10.0003C14.6665 9.77935 14.5788 9.56742 14.4226 9.41113L7.75589 2.74447C7.67814 2.66454 7.58514 2.60103 7.48239 2.55769C7.37965 2.51436 7.26926 2.49209 7.15775 2.49219Z"/></svg></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: false,
          },
        },
      ],
    });
  }
  // Слайдер сравнения
  if ($(".comparison__products").length) {
    $(".comparison__products")
      .find(".products__list")
      .slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        speed: 2000,
        autoplay: true,
        autoplaySpeed: 2000,
        variableWidth: true,
        draggable: false,
        swipe: false,
        swipeToSlide: false,
        touchMove: false,
        draggable: false,
        accessibility: false,
        appendArrows: ".comparison__navigation",
        prevArrow:
          '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 20" ><path d="M13.8423 17.5078C14.008 17.5076 14.1698 17.458 14.3072 17.3654C14.4446 17.2727 14.5513 17.1412 14.6135 16.9876C14.6758 16.8341 14.6909 16.6654 14.6569 16.5033C14.6228 16.3411 14.5412 16.1927 14.4225 16.0771L8.34502 9.99967L14.4225 3.9222C14.5025 3.84541 14.5663 3.75344 14.6103 3.65167C14.6543 3.54989 14.6775 3.44036 14.6787 3.3295C14.6798 3.21863 14.6588 3.10865 14.6169 3.006C14.575 2.90335 14.513 2.8101 14.4346 2.7317C14.3562 2.6533 14.263 2.59133 14.1603 2.54943C14.0577 2.50752 13.9477 2.48651 13.8368 2.48764C13.7259 2.48877 13.6164 2.51201 13.5146 2.556C13.4129 2.59998 13.3209 2.66384 13.2441 2.74382L6.57744 9.41048C6.42122 9.56677 6.33346 9.7787 6.33346 9.99967C6.33346 10.2207 6.42122 10.4326 6.57744 10.5889L13.2441 17.2555C13.3219 17.3355 13.4149 17.399 13.5176 17.4423C13.6203 17.4856 13.7307 17.5079 13.8423 17.5078Z"/></svg></div>',
        nextArrow:
          '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 20" ><path d="M7.15775 2.49219C6.99204 2.49239 6.83015 2.54199 6.69277 2.63465C6.55539 2.72731 6.44875 2.85881 6.38647 3.01237C6.32419 3.16593 6.3091 3.33457 6.34313 3.49675C6.37715 3.65893 6.45876 3.80728 6.57751 3.92285L12.655 10.0003L6.57751 16.0778C6.49753 16.1546 6.43368 16.2466 6.38969 16.3483C6.3457 16.4501 6.32246 16.5596 6.32133 16.6705C6.3202 16.7814 6.34121 16.8914 6.38312 16.994C6.42502 17.0966 6.48699 17.1899 6.56539 17.2683C6.64379 17.3467 6.73704 17.4087 6.83969 17.4506C6.94234 17.4925 7.05232 17.5135 7.16319 17.5124C7.27406 17.5112 7.38358 17.488 7.48536 17.444C7.58713 17.4 7.67911 17.3362 7.75589 17.2562L14.4226 10.5895C14.5788 10.4332 14.6665 10.2213 14.6665 10.0003C14.6665 9.77935 14.5788 9.56742 14.4226 9.41113L7.75589 2.74447C7.67814 2.66454 7.58514 2.60103 7.48239 2.55769C7.37965 2.51436 7.26926 2.49209 7.15775 2.49219Z"/></svg></div>',
        responsive: [
          {
            breakpoint: 767,
            settings: {
              variableWidth: false,
            },
          },
        ],
      });
  }
  // Карусель изображений
  if ($(".layout__collections").length) {
    $(".collections__list_primary").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      speed: 2000,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: true,

      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 20" ><path d="M13.8423 17.5078C14.008 17.5076 14.1698 17.458 14.3072 17.3654C14.4446 17.2727 14.5513 17.1412 14.6135 16.9876C14.6758 16.8341 14.6909 16.6654 14.6569 16.5033C14.6228 16.3411 14.5412 16.1927 14.4225 16.0771L8.34502 9.99967L14.4225 3.9222C14.5025 3.84541 14.5663 3.75344 14.6103 3.65167C14.6543 3.54989 14.6775 3.44036 14.6787 3.3295C14.6798 3.21863 14.6588 3.10865 14.6169 3.006C14.575 2.90335 14.513 2.8101 14.4346 2.7317C14.3562 2.6533 14.263 2.59133 14.1603 2.54943C14.0577 2.50752 13.9477 2.48651 13.8368 2.48764C13.7259 2.48877 13.6164 2.51201 13.5146 2.556C13.4129 2.59998 13.3209 2.66384 13.2441 2.74382L6.57744 9.41048C6.42122 9.56677 6.33346 9.7787 6.33346 9.99967C6.33346 10.2207 6.42122 10.4326 6.57744 10.5889L13.2441 17.2555C13.3219 17.3355 13.4149 17.399 13.5176 17.4423C13.6203 17.4856 13.7307 17.5079 13.8423 17.5078Z"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 20" ><path d="M7.15775 2.49219C6.99204 2.49239 6.83015 2.54199 6.69277 2.63465C6.55539 2.72731 6.44875 2.85881 6.38647 3.01237C6.32419 3.16593 6.3091 3.33457 6.34313 3.49675C6.37715 3.65893 6.45876 3.80728 6.57751 3.92285L12.655 10.0003L6.57751 16.0778C6.49753 16.1546 6.43368 16.2466 6.38969 16.3483C6.3457 16.4501 6.32246 16.5596 6.32133 16.6705C6.3202 16.7814 6.34121 16.8914 6.38312 16.994C6.42502 17.0966 6.48699 17.1899 6.56539 17.2683C6.64379 17.3467 6.73704 17.4087 6.83969 17.4506C6.94234 17.4925 7.05232 17.5135 7.16319 17.5124C7.27406 17.5112 7.38358 17.488 7.48536 17.444C7.58713 17.4 7.67911 17.3362 7.75589 17.2562L14.4226 10.5895C14.5788 10.4332 14.6665 10.2213 14.6665 10.0003C14.6665 9.77935 14.5788 9.56742 14.4226 9.41113L7.75589 2.74447C7.67814 2.66454 7.58514 2.60103 7.48239 2.55769C7.37965 2.51436 7.26926 2.49209 7.15775 2.49219Z"/></svg></div>',

      asNavFor: ".collections__list_secondary",
    });
    $(".collections__list_secondary").slick({
      slidesToShow: 9,
      slidesToScroll: 1,
      speed: 2000,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      asNavFor: ".collections__list_primary",
      centerMode: true,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 8,
            slidesToScroll: 1,
          },
        },
      ],

      responsive: [
        {
          breakpoint: 580,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }
  // Меню
  navInit();
  function navInit() {
    header.find(".header__burger").on("click", function () {
      $(this).closest(header).toggleClass("layout__header_menu-active");
    });
  }
  // Анимации при скролле страницы
  $(window).on("load resize scroll", function () {
    let h = $(window).height();
    scrollHeader(h);
    scrollSection(h);
    scrollImage(h);
  });
  function scrollHeader(h) {
    if ($(window).scrollTop() >= 1) {
      header.addClass("layout__header_scroll");
    } else {
      header.removeClass("layout__header_scroll");
    }
  }
  function scrollSection(h) {
    let section = $(".section");
    section.each(function () {
      if ($(window).scrollTop() + h >= $(this).offset().top) {
        $(this).addClass("section_animation");
      }
    });
  }
  function scrollImage(h) {
    // Image initialization
    let img = $("img");
    img.each(function () {
      if (
        $(window).scrollTop() + 1.5 * h >= $(this).offset().top &&
        this.getAttribute("data-src") &&
        this.src !== this.getAttribute("data-src")
      ) {
        this.src = this.getAttribute("data-src");
      }
    });
  }
  // Табы
  if ($(".section__tabs").length) {
    sectionTabsInit();
  }
  function sectionTabsInit() {
    let position,
      tabsActive = "tabs__item_active";
    $(".section__tabs").on("click", ".tabs__item", function () {
      position = $(this).index();
      $(this).addClass(tabsActive).siblings().removeClass(tabsActive);
      $(this)
        .closest(".layout__section")
        .find(".section__main")
        .find(".tabs__item")
        .eq(position)
        .slideDown(400)
        .siblings()
        .slideUp(400);
      $(this)
        .closest(".layout__section")
        .find(".section__action")
        .find(".tabs__item")
        .eq(position)
        .slideDown(400)
        .siblings()
        .slideUp(400);
    });
  }
  // Табы
  if ($(".tabs").length) {
    tabsInit();
  }
  function tabsInit() {
    let position,
      tabsBodyItem = $(".tabs__main").find(".tabs__item"),
      tabsActive = "tabs__item_active";
    $(".tabs__header").on("click", ".tabs__item", function () {
      position = $(this).index();
      $(this).addClass(tabsActive).siblings().removeClass(tabsActive);
      tabsBodyItem
        .eq(position)
        .addClass(tabsActive)
        .siblings()
        .removeClass(tabsActive);
    });
    $(".tabs").on("click", ".tabs__header", function () {
      $(this).toggleClass("tabs__header_active");
    });
  }
  // Добавление файла
  if ($(".reservation__formular").length) {
    attachInit();
  }
  function attachInit() {
    $(".reservation__formular").on("click", ".form__toggle", function () {
      $(".form__group_tertiary").slideToggle(400);
    });
    // change
    $(".form__attach").on("change", ".attach__input", function (e) {
      let element = $(this);
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          element
            .closest(".form__attach")
            .addClass("form__attach_active")
            .find(".attach__img")
            .attr("src", e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
    $(".form__attach").on("click", ".attach__remove", function (e) {
      e.stopPropagation();
      $(this).closest(".form__attach").removeClass("form__attach_active");
    });
  }
  // Фильтр
  if ($(".category__filter").length) {
    filterInit();
  }
  function filterInit() {
    $(".category__header").on("click", ".category__filter-toggle", function () {
      $(this).toggleClass("active");
      $(".category__filter").slideToggle(400);
    });

    $(document).on("click", ".filter__action", function () {
      $(".category__filter-toggle").removeClass("active");
      $(".category__filter").slideUp(400);
    });
    $(window).keydown(function (e) {
      if (e.key === "Escape") {
        $(".category__filter-toggle").removeClass("active");
        $(".category__filter").slideUp(400);
      }
    });
  }

  // Бренды
  if ($(".category__brands").length) {
    brandsInit();
  }
  function brandsInit() {
    $(".category__header").on("click", ".category__brands-toggle", function () {
      $(this).toggleClass("active");
      $(".category__brands").slideToggle(400);
    });

    $(document).on("click", ".brands__action", function () {
      $(".category__brands-toggle").removeClass("active");
      $(".category__brands").slideUp(400);
    });
    $(window).keydown(function (e) {
      if (e.key === "Escape") {
        $(".category__brands-toggle").removeClass("active");
        $(".category__brands").slideUp(400);
      }
    });
  }

  // Поиск
  if ($(".header__search").length) {
    searchInit();
  }
  function searchInit() {
    $(".header__action").on("click", ".action__item_search", function () {
      $(".layout__header").toggleClass("layout__header_search");
      $(this).toggleClass("action__item_active");
      $(".header__search").toggleClass("header__search_active");
    });
  }
  // Аккордион
  if ($(".layout__faq").length) {
    faqInit();
  }
  function faqInit() {
    $(".layout__faq").on("click", ".faq__header", function () {
      $(this).closest(".faq__item").toggleClass("faq__item_active");
      $(this).closest(".faq__item").find(".faq__main").slideToggle(400);
    });
  }
  // Форма
  if ($("form").length) {
    jcf.setOptions("Select", {
      wrapNative: false,
      wrapNativeOnMobile: false,
      fakeDropInBody: false,
      maxVisibleItems: 5,
    });
    jcf.replaceAll();
    $(".form__input_mask").mask("8(999) 999-99-99");
    /* Ползунок */
    if ($(".category__filter").length) {
      rangeInit();
    }
    function rangeInit() {
      $(".form__range_primary")
        .find(".range__slider")
        .slider({
          range: true,
          min: 0,
          max: 40000000,
          step: 100,
          slide: function (event, ui) {
            $(".form__range_primary")
              .find(".range__field_min")
              .val(ui.values[0]);
            $(".form__range_primary")
              .find(".range__field_max")
              .val(ui.values[1]);
          },
        });
      $(".form__range_secondary")
        .find(".range__slider")
        .slider({
          range: true,
          min: 2,
          max: 8,
          step: 1,
          slide: function (event, ui) {
            console.log(ui.values[0]);
            $(".form__range_secondary")
              .find(".range__field_min")
              .val(ui.values[0]);
            $(".form__range_secondary")
              .find(".range__field_max")
              .val(ui.values[1]);
          },
        });
      $(".form__range_tertiary")
        .find(".range__slider")
        .slider({
          range: true,
          min: 100,
          max: 800,
          step: 100,

          slide: function (event, ui) {
            console.log(ui.values[0]);
            $(".form__range_tertiary")
              .find(".range__field_min")
              .val(ui.values[0]);
            $(".form__range_tertiary")
              .find(".range__field_max")
              .val(ui.values[1]);
          },
        });
    }
    if ($(".buying__calc").length) {
      buyingRange();
    }
    function buyingRange() {
      $(".buying__calc").find(".range__slider").slider({
        range: "min",
      });
    }
    /* Датапикер */
    if ($(".form__datepicker").length) {
      datepickerInit();
    }
    function datepickerInit() {
      $.datepicker.regional["ru"] = {
        closeText: "Закрыть",
        currentText: "Сегодня",
        prevText:
          '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none"><path d="M13 4L7 10L13 16" stroke="#7A828A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        nextText:
          '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none"><path d="M7 16L13 10L7 4" stroke="#7A828A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        monthNames: [
          "Январь",
          "Февраль",
          "Март",
          "Апрель",
          "Май",
          "Июнь",
          "Июль",
          "Август",
          "Сентябрь",
          "Октябрь",
          "Ноябрь",
          "Декабрь",
        ],
        monthNamesShort: [
          "Янв",
          "Фев",
          "Мар",
          "Апр",
          "Май",
          "Июн",
          "Июл",
          "Авг",
          "Сен",
          "Окт",
          "Ноя",
          "Дек",
        ],
        dayNames: [
          "воскресенье",
          "понедельник",
          "вторник",
          "среда",
          "четверг",
          "пятница",
          "суббота",
        ],
        dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        weekHeader: "Не",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: "",
      };
      $.datepicker.setDefaults($.datepicker.regional["ru"]);
      $(".form__datepicker").datepicker({
        showOtherMonths: true,
        minDate: 0,
      });
    }
  }
  // Модальное окно
  modalInit();
  function modalInit() {
    let modalName;
    $(document).on("click", ".modal-init", function () {
      layout
        .addClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
      modalName = $(this).data("modalname");
      layout.find("." + modalName + "").addClass("modal__layout_active");
    });
    $(document).mouseup(function (e) {
      if ($(".modal__layout_active").length) {
        var div = $(".modal__layout");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
          modalHide();
        }
      }
    });
    $(document).on("click", ".modal__action", function () {
      modalHide();
    });
    $(window).keydown(function (e) {
      if (e.key === "Escape") {
        modalHide();
      }
    });
    function modalHide() {
      layout
        .removeClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
    }
  }
  // Изменение темы
  if ($(".header__theme").length) {
    themeInit();
  }
  function themeInit() {
    $(".layout__header").on("click", ".header__theme", function () {
      $(this).toggleClass("header__theme_active");
      $("body").toggleClass("dark");
    });
  }
})(jQuery);
